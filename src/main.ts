import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as basicAuth from 'express-basic-auth';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  // Assets
  app.useStaticAssets(join(__dirname, '..', 'uploads'));

  // Validation
  app.useGlobalPipes(new ValidationPipe());

  // Protect auth
  app.use(
    `/docs`,
    basicAuth({
      challenge: true,
      users: {
        [process.env.SWAGGER_USER]: process.env.SWAGGER_PASSWORD,
        admin: 'Admin1@34',
      },
    }),
  );

  // API Documentation
  const config = new DocumentBuilder()
    .setTitle('Content Management API')
    .setDescription('The Content API description')
    .setVersion('1.0')
    .addTag('contents')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  await app.listen(3000);
}
bootstrap();
