import { HttpStatus } from 'aws-sdk/clients/lambda';

export type IResponse<T> = {
  data: T;
  code: HttpStatus;
  message?: string;
};
