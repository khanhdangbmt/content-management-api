import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmConfigService } from './TypeOrmConfigService';
import { DataSource, DataSourceOptions } from 'typeorm';

@Global()
@Module({})
export class DatabaseModule {
  static registerMySQL() {
    return TypeOrmModule.forRootAsync({
      useClass: TypeOrmConfigService,
      dataSourceFactory: async (options: DataSourceOptions) => {
        return new DataSource(options).initialize();
      },
    });
  }
}
