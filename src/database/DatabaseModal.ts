export enum DatabaseModal {
  DBConnection = 'DBConnection',
  Task = 'task',
  Project = 'project',
  Role = 'role',
  User = 'user',
  Permission = 'permission',
}
