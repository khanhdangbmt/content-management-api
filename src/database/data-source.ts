import { DataSource, DataSourceOptions } from 'typeorm';

export const AppDataSource = new DataSource({
  type: 'mysql',
  host: process.env.DB_HOST,
  port: 3306,
  username: 'root',
  password: 'Admin1@34',
  database: 'nest-base',
  migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
}) as unknown as DataSourceOptions;
