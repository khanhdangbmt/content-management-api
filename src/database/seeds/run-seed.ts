import { NestFactory } from '@nestjs/core';
// import { RoleSeedService } from './role/role-seed.service';
import { SeedModule } from './seeds.module';
// import { PermissionSeedService } from './permission/service';

const runSeed = async () => {
  const app = await NestFactory.create(SeedModule);

  // run
  // await app.get(RoleSeedService).run();
  // await app.get(PermissionSeedService).run();

  await app.close();
};

void runSeed();
