import { MigrationInterface, QueryRunner } from 'typeorm';

export class Init1711256250177 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'CREATE TABLE `role` ( `id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(255) NOT NULL, CONSTRAINT PK_role_id PRIMARY KEY (`id`));',
    );

    await queryRunner.query(
      'CREATE TABLE users (id INT NOT NULL AUTO_INCREMENT, email VARCHAR(255), password VARCHAR(255), firstName VARCHAR(255), lastName VARCHAR(255), roleId INT, isActive BOOL default true, createdAt TIMESTAMP  DEFAULT NOW() , updatedAt TIMESTAMP  DEFAULT NOW() , deletedAt TIMESTAMP  DEFAULT NULL, CONSTRAINT fk_user_role FOREIGN KEY (roleId) REFERENCES role(id),PRIMARY KEY (id) )',
    );

    await queryRunner.query(
      'CREATE TABLE session (id INT NOT NULL AUTO_INCREMENT, hash VARCHAR(255) NOT NULL, createdAt TIMESTAMP NOT NULL DEFAULT NOW(), updatedAt TIMESTAMP NOT NULL DEFAULT NOW(), deletedAt TIMESTAMP, userId INT, FOREIGN KEY (userId) REFERENCES users(id),PRIMARY KEY (id) )',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "roles"`);
    await queryRunner.query(`DROP TABLE "session"`);
    await queryRunner.query(`DROP TABLE "users"`);
    await queryRunner.query(
      `ALTER TABLE "user" DROP CONSTRAINT "FK_dc18daa696860586ba4667a9d31"`,
    );
  }
}
