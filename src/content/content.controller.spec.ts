import { Test, TestingModule } from '@nestjs/testing';
import { ContentController } from './content.controller';
import { ContentService } from './content.service';
import * as request from 'supertest';
import { HttpStatus, INestApplication } from '@nestjs/common';

describe('ContentController', () => {
  let app: INestApplication;
  const ContentServiceMock = {
    create: jest.fn().mockImplementation((content) => content),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ContentController],
      providers: [
        {
          provide: ContentService,
          useValue: ContentServiceMock,
        },
      ],
    }).compile();

    app = module.createNestApplication();
    await app.init();
  });

  describe('/ POST content', () => {
    it('should upload a file successfully', async () => {
      const response = await request(app.getHttpServer())
        .post('/content')
        .attach('file', 'test/files/test_image.png');

      expect(response.status).toBe(HttpStatus.CREATED);
      expect(response.body.data).toHaveProperty('filename');
      expect(response.body.data).toHaveProperty('url');
      expect(response.body.data).toHaveProperty('size');
      expect(response.body.data).toHaveProperty('mimetype');
      expect(response.body.data.type).toBe('image');
      expect(ContentServiceMock.create).toHaveBeenCalled();
    });

    it('should throw BadRequestException if no file is provided', async () => {
      const response = await request(app.getHttpServer()).post('/content');

      expect(response.status).toBe(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual('File is not provided');
      expect(response.body.statusCode).toBe(HttpStatus.BAD_REQUEST);
    });
  });
});
