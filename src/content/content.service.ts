import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Content } from './entities/content.entity';

@Injectable()
export class ContentService {
  constructor(
    @InjectRepository(Content)
    private contentRepository: Repository<Content>,
  ) {}

  async findOne(id: number): Promise<Content> {
    try {
      const result = await this.contentRepository.findOne({
        where: {
          id,
        },
      });
      return result;
    } catch (err) {
      throw new HttpException(err, HttpStatus.NOT_FOUND);
    }
  }

  async create(content: Partial<Content>): Promise<Content> {
    try {
      const newContent = this.contentRepository.create(content);
      return this.contentRepository.save(newContent);
    } catch (e) {
      throw new HttpException('Create content error', HttpStatus.BAD_REQUEST);
    }
  }
}
