import { Test, TestingModule } from '@nestjs/testing';
import { ContentService } from './content.service';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { HttpException, HttpStatus } from '@nestjs/common';
import { Content } from './entities/content.entity';

describe('ContentService', () => {
  let service: ContentService;
  let repository: Repository<Content>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ContentService,
        {
          provide: getRepositoryToken(Content),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<ContentService>(ContentService);
    repository = module.get<Repository<Content>>(getRepositoryToken(Content));
  });

  describe('findOne', () => {
    it('should return the content if found', async () => {
      const contentId = 1;
      const content: Content = {
        id: contentId,
        type: 'application',
        filename: '1718336711016-55198393.pdf',
        mimetype: 'application/pdf',
        size: 407483,
        url: '/uploads/1718336711016-55198393.pdf',
      };

      jest.spyOn(repository, 'findOne').mockResolvedValue(content);

      expect(await service.findOne(contentId)).toEqual(content);
    });

    it('should throw HttpException if not found', async () => {
      const contentId = 2;

      jest.spyOn(repository, 'findOne').mockResolvedValue(null);
      const resultFind = await service.findOne(contentId);
      expect(resultFind).toEqual(null);
    });
  });

  describe('create', () => {
    it('should create and return the new content', async () => {
      const content: Content = {
        id: 1,
        type: 'application',
        filename: '1718336711016-55198393.pdf',
        mimetype: 'application/pdf',
        size: 407483,
        url: '/uploads/1718336711016-55198393.pdf',
      };
      const savedContent: Content = { id: 2, ...content };

      jest.spyOn(repository, 'create').mockReturnValue(savedContent as Content);
      jest.spyOn(repository, 'save').mockResolvedValue(savedContent as Content);

      expect(await service.create(content)).toEqual(savedContent);
    });

    it('should throw HttpException if creation fails', async () => {
      const content = {
        id: 3,
        type: 'application',
        filename: '1718336711016-55198393.pdf',
        mimetype: 'application/pdf',
        size: 407483,
        url: '/uploads/1718336711016-55198393.pdf',
      };

      jest.spyOn(repository, 'create').mockReturnValue(content);
      jest
        .spyOn(repository, 'save')
        .mockRejectedValue(new Error('Create content error'));

      await expect(service.create(content)).rejects.toThrow(
        new HttpException('Create content error', HttpStatus.BAD_REQUEST),
      );
    });
  });
});
