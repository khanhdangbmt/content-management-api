import {
  Controller,
  Get,
  Post,
  Param,
  UploadedFile,
  UseInterceptors,
  BadRequestException,
  HttpStatus,
  NotFoundException,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ContentService } from './content.service';
import { Content } from './entities/content.entity';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { IResponse } from 'src/utils/response';
import { ApiBody, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';

@Controller('content')
@ApiTags('Content')
export class ContentController {
  constructor(private readonly contentService: ContentService) {}

  @Get(':id')
  @ApiOperation({ summary: 'Get content details' })
  async getContent(@Param('id') id: number): Promise<IResponse<Content>> {
    const content = await this.contentService.findOne(id);
    if (!content) {
      throw new NotFoundException('Content not found');
    }
    return {
      data: content,
      code: HttpStatus.OK,
    };
  }

  @Post('')
  @ApiOperation({ summary: 'Upload a file' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          cb(null, `${uniqueSuffix}${extname(file.originalname)}`);
        },
      }),
      fileFilter: (req, file, cb) => {
        const filetypes = /jpeg|jpg|png|pdf|mp4/;
        const mimetype = filetypes.test(file.mimetype);
        const extnameUse = filetypes.test(
          extname(file.originalname).toLowerCase(),
        );

        if (mimetype && extnameUse) {
          return cb(null, true);
        } else {
          cb(new BadRequestException('Invalid file type'), false);
        }
      },
      limits: { fileSize: 10 * 1024 * 1024 },
    }),
  )
  async uploadFile(
    @UploadedFile() file: Express.Multer.File,
  ): Promise<IResponse<Content>> {
    if (!file) {
      throw new BadRequestException('File is not provided');
    }

    const content = {
      type: file.mimetype.split('/')[0],
      filename: file.filename,
      mimetype: file.mimetype,
      size: file.size,
      url: `/uploads/${file.filename}`,
    };

    const result = await this.contentService.create(content);
    return {
      data: result,
      code: HttpStatus.OK,
    };
  }
}
