import { IsNumber, IsString, IsOptional } from 'class-validator';
import { Content } from '../entities/content.entity';

export class CreateContentDto implements Content {
  @IsOptional()
  id: number;

  @IsString()
  type: string;

  @IsString()
  filename: string;

  @IsString()
  mimetype: string;

  @IsNumber()
  size: number;

  @IsString()
  url: string;
}
