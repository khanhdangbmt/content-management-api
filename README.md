# content-management-api

## Features:
- Upload content
- Get content
- Upload directly to server (will enhance it later when we need)


## Application assumption
- No need to login, register, authentication, authorization
- Focus on upload and retrieve content.


## Application Environment
Documentations: /docs

```javascript
DB_TYPE=
DB_HOST=
DB_PORT=
DB_USERNAME=
DB_PASSWORD=
DB_NAME=
DB_SYNCHRONIZE=true # using for sync db

// /docs authentication
SWAGGER_USER= 
SWAGGER_PASSWORD= 
```

## Result 
### API Documentations
![alt text](image-2.png)

### Upload content
http://localhost:3000/content
![alt text](image.png)

### Get content
http://localhost:3000/content/{content-id}
![alt text](image-1.png)